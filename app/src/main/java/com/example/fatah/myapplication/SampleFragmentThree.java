package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SampleFragmentThree extends Fragment {

    public SampleFragmentThree newInstance(String string) {
        SampleFragmentThree sampleFragmentThree = new SampleFragmentThree();

        Bundle bundle = new Bundle();
        bundle.putString("val",string);
        sampleFragmentThree.setArguments(bundle);

        return sampleFragmentThree;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_sample_fragment_three, container, false);

        Button button = (Button) rootView.findViewById(R.id.button_fragment2_passvar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView textView = (TextView) rootView.findViewById(R.id.textview_fragment_passvar);
                try{
                    if(getArguments().getString("val").length() == 0){
                        Toast.makeText(getActivity(), "kosong om!!", Toast.LENGTH_SHORT).show();
                    }else{
                        textView.setText(getArguments().getString("val"));
                    }
                }catch(NullPointerException e){
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        return rootView;
    }

}
