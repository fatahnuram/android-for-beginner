package com.example.fatah.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by fatah on 13/02/17.
 */

// class for header holder
public class HeaderNewsHolder extends RecyclerView.ViewHolder {
    // variable declarations
    View item;
    TextView tgl, nama;
    ImageView img;

    // constructor
    public HeaderNewsHolder(View itemView) {
        super(itemView);
        // init view
        item = itemView;
        tgl = (TextView) itemView.findViewById(R.id.recycler_header_date);
        nama = (TextView) itemView.findViewById(R.id.recycler_header_text);
        img = (ImageView) itemView.findViewById(R.id.recycler_header_img);
    }

    // method for binding view into list
    public void bindView(Context context, ArrayList<RecyclerItemMovie> news, int position){
        // get the position
        final RecyclerItemMovie item = news.get(position);

        // set text into item
        tgl.setText(item.getTgl());
        nama.setText(item.getNama());
        // load image into item using picasso
        Picasso.with(context).load(item.getImg()).into(img);

        // set click function each item
        this.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // declare bundle
                Bundle bundle = new Bundle();
                // passing news id into bundle
                bundle.putString("id", item.getId());
                // send the bundle using EventBus
                EventBus.getDefault().post(bundle);
            }
        });
    }
}
