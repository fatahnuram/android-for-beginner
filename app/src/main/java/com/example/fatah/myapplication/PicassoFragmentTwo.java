package com.example.fatah.myapplication;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class PicassoFragmentTwo extends Fragment {
    PicassoListAdapter picassoListAdapter;
    Context context;
    AsyncHttpClient client;
    ArrayList<HashMap<String,String>> arrayList;
    ListView listView;
    JSONArray array;
    JSONObject object;

    public PicassoFragmentTwo newInstance(Context context){
        PicassoFragmentTwo picassoFragmentTwo = new PicassoFragmentTwo();
        this.context = context;
        return picassoFragmentTwo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picasso_fragment_two, container, false);

        client = new AsyncHttpClient();
        arrayList = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.listview_picasso);

        getData();

        return view;
    }

    public void getData(){
        String url = "http://www.androidbegin.com/tutorial/jsonparsetutorial.txt";

        client.get(context, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody, Charset.forName("UTF8"));

                try{
                    object = new JSONObject(response);
                    array = object.getJSONArray("worldpopulation");

                    for(int i=0; i<array.length(); i++){
                        HashMap<String,String> map = new HashMap<>();
                        object = array.getJSONObject(i);

                        map.put("rank", object.getString("rank"));
                        map.put("country", object.getString("country"));
                        map.put("population", object.getString("population"));
                        map.put("flag", object.getString("flag"));

                        arrayList.add(map);
                    }

                    setView();

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getContext(), "Error connection", Toast.LENGTH_SHORT);
            }
        });
    }

    private void setView(){
        picassoListAdapter = new PicassoListAdapter(getActivity(), arrayList);
        listView.setAdapter(picassoListAdapter);
    }
}