package com.example.fatah.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by fatah on 06/02/17.
 */

// adapter for drawer item listing
public class DrawerListAdapter extends BaseAdapter {
    // variable declarations
    private Context context;
    private LayoutInflater inflater;
    private DrawerListModel[] model;

    // constructor, matching required objects
    public DrawerListAdapter(Context context, DrawerListModel[] model){
        this.context = context;
        this.model = model;
    }

    // return the number of the drawer items
    @Override
    public int getCount() {
        return model.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    //
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // get the main inflater from context
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the parent view
        View view = inflater.inflate(R.layout.drawer_list_item, parent, false);

        // init view
        TextView textView = (TextView) view.findViewById(R.id.drawer_list_name);
        ImageView imageView = (ImageView) view.findViewById(R.id.drawer_list_icon);

        // set text and set image for each model item
        textView.setText(""+model[position].getName());
        imageView.setImageResource(model[position].getIcon());

        // click function using EventBus, regarding its position
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // init bundle for container
                Bundle bundle = new Bundle();
                // pass the position to EventBus
                bundle.putInt("posisi", position);
                // send the bundle into parent activity
                EventBus.getDefault().post(bundle);
            }
        });

        return view;
    }
}