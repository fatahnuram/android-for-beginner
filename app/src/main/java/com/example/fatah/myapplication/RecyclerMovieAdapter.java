package com.example.fatah.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by fatah on 07/02/17.
 */

public class RecyclerMovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<RecyclerItemMovie> news;
    private Context context;

    public RecyclerMovieAdapter(Context context, ArrayList<RecyclerItemMovie> recyclerItemMovie){
        this.context = context;
        news = recyclerItemMovie;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_header_news, parent, false);
            return new HeaderNewsHolder(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_movie, parent, false);
            return new RecyclerMovieHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HeaderNewsHolder){
            HeaderNewsHolder headerNewsHolder = (HeaderNewsHolder) holder;
            headerNewsHolder.bindView(context, news, position);
        }else if(holder instanceof RecyclerMovieHolder){
            RecyclerMovieHolder recyclerMovieHolder = (RecyclerMovieHolder) holder;
            recyclerMovieHolder.bindView(context, news, position);
        }
    }

    @Override
    public int getItemViewType(int position){
        return position == 0 ? 0 : 1 ;
    }

    @Override
    public int getItemCount() {
        return news.size();
    }
}
