package com.example.fatah.myapplication;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PagerActivity extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

        String string = getIntent().getExtras().getString("val");

        viewPager = (ViewPager) findViewById(R.id.activity_pager);
        viewPager.setAdapter(new SamplePagerAdapter(getSupportFragmentManager(), string));
    }
}
