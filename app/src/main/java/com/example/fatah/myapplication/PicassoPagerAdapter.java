package com.example.fatah.myapplication;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by fatah on 27/01/17.
 */

public class PicassoPagerAdapter extends FragmentPagerAdapter {
    Context context;

    public PicassoPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return new PicassoFragment().newInstance(context);
        }else{
            return new PicassoFragmentTwo().newInstance(context);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
