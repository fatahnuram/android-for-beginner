package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// class fragment for fragment number one
public class DrawerFragmentOne extends Fragment {
    // function init fragment
    public DrawerFragmentOne newInstance() {
        DrawerFragmentOne drawerFragmentOne = new DrawerFragmentOne();
        return drawerFragmentOne;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer_fragment_one, container, false);
    }
}
