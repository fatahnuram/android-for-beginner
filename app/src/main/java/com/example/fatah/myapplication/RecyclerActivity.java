package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class RecyclerActivity extends AppCompatActivity {
    private ArrayList<RecyclerItemMovie> news = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerMovieAdapter adapter;
    private AsyncHttpClient client = new AsyncHttpClient();
    private JSONObject object;
    private JSONArray array;

    @Override
    protected void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(Bundle bundle){
        if(bundle.containsKey("id")){
            Intent i = new Intent(RecyclerActivity.this, RecyclerItemDetailActivity.class);
            i.putExtra("id", bundle.getString("id"));
            startActivity(i);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        getData();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void getData(){
        String url = "http://ugm.ac.id/berita";

        client.get(RecyclerActivity.this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody, Charset.forName("UTF8"));

                try{
                    object = new JSONObject(response);
                    array = object.getJSONArray("item");

                    //setHeader();

                    for(int i=0; i<array.length(); i++){
                        RecyclerItemMovie item = new RecyclerItemMovie();
                        object = array.getJSONObject(i);

                        item.setId(object.getString("id"));
                        item.setNama(object.getString("nama"));
                        item.setDesc(object.getString("desc"));
                        item.setTgl(object.getString("tgl"));
                        item.setImg(object.getString("gambar"));

                        news.add(item);
                    }

                    adapter = new RecyclerMovieAdapter(RecyclerActivity.this, news);
                    recyclerView.setAdapter(adapter);

                }catch (JSONException e){
                    Toast.makeText(RecyclerActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(RecyclerActivity.this, "Error connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
    /*
    private void setHeader(){
        try{
            RecyclerItemMovie header = new RecyclerItemMovie();
            object = array.getJSONObject(0);

            header.setId(object.getString("id"));
            header.setImg(object.getString("gambar"));
            header.setTgl(object.getString("tgl"));
            header.setNama(object.getString("nama"));

            news.add(header);
        }catch (JSONException e){
            Toast.makeText(RecyclerActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }*/
}
