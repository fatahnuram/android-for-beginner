package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PagerLanderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager_lander);

        Button button = (Button) findViewById(R.id.button_fragment_passvar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.edittext_fragment_passvar);
                String val = editText.getText().toString();

                Intent i = new Intent(PagerLanderActivity.this, PagerActivity.class);
                i.putExtra("val", val);
                startActivity(i);
            }
        });
    }
}
