package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// class fragment for home
public class DrawerFragment extends Fragment {
    // function for init fragment
    public DrawerFragment newInstance() {
        DrawerFragment drawerFragment = new DrawerFragment();
        return drawerFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate the layout, and return (show) to screen
        View view = inflater.inflate(R.layout.fragment_drawer, container, false);
        return view;
    }
}
