package com.example.fatah.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by fatah on 25/01/17.
 */

public class SamplePagerAdapter extends FragmentPagerAdapter {
    // variable declarations
    String string;

    // constructor
    public SamplePagerAdapter(FragmentManager fragmentManager, String string){
        super(fragmentManager);
        this.string = string;
    }

    // select fragment by position to be shown into screen
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                // if postition 0, use default fragment
                return new SampleFragment().newInstance();
            case 1:
                // if postition 1, use fragment number two
                return new SampleFragmentTwo().newInstance();
            default:
                // else, use fragment number three, passing a string
                return new SampleFragmentThree().newInstance(string);
        }
    }

    // return number of items
    @Override
    public int getCount() {
        return 3;
    }
}
