package com.example.fatah.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PermActivity extends AppCompatActivity {
    // variable declarations
    private Button buttonCheck, buttonRequest;
    private int permCoarseLoc/*, permFineLoc*/;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perm);

        // init permissions
        permCoarseLoc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        //permFineLoc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        // init buttons
        buttonCheck = (Button) findViewById(R.id.button_perm_check);
        buttonRequest = (Button) findViewById(R.id.button_perm_request);

        // set click function on button check
        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check the location permissions
                if(permCoarseLoc != PackageManager.PERMISSION_GRANTED
                        /*&& permFineLoc != PackageManager.PERMISSION_GRANTED*/){
                    // permissions not granted, show to screen
                    Toast.makeText(PermActivity.this, "Permissions not granted", Toast.LENGTH_SHORT).show();
                }
                if(permCoarseLoc == PackageManager.PERMISSION_GRANTED
                        /*&& permFineLoc == PackageManager.PERMISSION_GRANTED*/){
                    // permissions granted, show to screen
                    Toast.makeText(PermActivity.this, "Permissions granted", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // set click function on button request
        buttonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // make permission popup request
                ActivityCompat.requestPermissions(PermActivity.this, new String[]{
                        // list of the permission that we want to be asked
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, 1);
            }
        });
    }

    // functions for request permission handling
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // check the request code accepted
        if(requestCode == 1){
            // check if the length of the result array is greater than 0 and permission is granted
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // permissions granted, update the variable permission and show status to screen
                permCoarseLoc = PackageManager.PERMISSION_GRANTED;
                Toast.makeText(PermActivity.this, "Permissions granted", Toast.LENGTH_SHORT).show();
            }else{
                // permissions not granted, show to screen
                Toast.makeText(PermActivity.this, "Permissions not granted", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
