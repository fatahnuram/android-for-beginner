package com.example.fatah.myapplication;

/**
 * Created by fatah on 11/02/17.
 */

// class for drawer item model
public class DrawerListModel {
    // variable declarations
    private int icon;
    private String name;

    // constructor
    public DrawerListModel(int icon, String name){
        this.icon = icon;
        this.name = name;
    }

    // method getter and setter
    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
