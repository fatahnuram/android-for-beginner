package com.example.fatah.myapplication;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class ListActivity extends AppCompatActivity {
    // variable declarations
    private AsyncHttpClient client;
    private ArrayList<HashMap<String, String>> list;
    private ListView listView;
    private JSONObject object;
    private JSONArray array;
    private ShopListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // init asynchttp
        client = new AsyncHttpClient();
        list = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listview_cart);

        // get data from online API
        getData();
    }

    public void getData(){
        // url containing json to parse
        String url = "http://www.androidbegin.com/tutorial/jsonparsetutorial.txt";

        // get data using method get
        client.get(ListActivity.this, url, new AsyncHttpResponseHandler() {
            // if success fetching data from internet
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                //Log.i("Response", responseBody + "");
                // parse response packet into readable string
                String response = new String(responseBody, Charset.forName("UTF8"));

                try{
                    // make json object from string response
                    object = new JSONObject(response);
                    // get array from json object, named worldpopulation
                    array = object.getJSONArray("worldpopulation");

                    // adds each item in the array into a new hashmap
                    for(int i=0; i<array.length(); i++){
                        // declare and init hashmap to be used
                        HashMap<String,String> map = new HashMap<>();
                        // get json object from array
                        object = array.getJSONObject(i);

                        // put string from json object labeled rank into hashmap using key rank
                        map.put("rank", object.getString("rank"));
                        // put string from json object labeled country into hashmap using key country
                        map.put("country", object.getString("country"));
                        // put string from json object labeled population into hashmap using key population
                        map.put("population", object.getString("population"));
                        // put string from json object labeled flag into hashmap using key flag
                        map.put("flag", object.getString("flag"));

                        // add hashmap into arraylist
                        list.add(map);
                    }

                    // attach list item into screen
                    setView();

                }catch(JSONException e){
                    // if exception occured, show error to screen
                    Toast.makeText(ListActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            // if failure to fetch data from internet
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // show to screen
                Toast.makeText(ListActivity.this, "Error connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // function for applying list item to screen
    public void setView(){
        // init adapter
        adapter = new ShopListAdapter(ListActivity.this, list);
        // set adapter
        listView.setAdapter(adapter);
    }
}
