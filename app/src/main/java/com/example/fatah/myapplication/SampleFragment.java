package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SampleFragment extends Fragment {
    // function to init fragment
    public SampleFragment newInstance(){
        SampleFragment sampleFragment = new SampleFragment();
        return sampleFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        // inflate the parent view
        View rootView = inflater.inflate(R.layout.fragment_sample, container, false);
        return rootView;
    }
}
