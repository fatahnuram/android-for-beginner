package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // click function for 1st button
        Button button1 = (Button) findViewById(R.id.latihan1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // make and init a new intent
                Intent i = new Intent(MainActivity.this, HelloWorldActivity.class);
                // start the intent
                startActivity(i);
            }
        });

        // click function for 2nd button
        Button button2 = (Button) findViewById(R.id.latihan2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, PrintVarActivity.class);
                startActivity(i);
            }
        });

        // click function for 3rd button
        Button button3 = (Button) findViewById(R.id.latihan3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ConversionActivity.class);
                startActivity(i);
            }
        });

        // click function for 4th button
        Button button4 = (Button) findViewById(R.id.latihan4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, PindahActivity.class);
                startActivity(i);
            }
        });

        // click function for 5th button
        Button button5 = (Button) findViewById(R.id.latihan5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PagerLanderActivity.class);
                startActivity(i);
            }
        });

        // click function for 6th button
        Button button6 = (Button) findViewById(R.id.latihan6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivity(i);
            }
        });

        // click function for 7th button
        Button button7 = (Button) findViewById(R.id.latihan7);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PicassoLanderActivity.class);
                startActivity(i);
            }
        });

        // click function for 8th button
        Button button8 = (Button) findViewById(R.id.latihan8);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, DrawerActivity.class);
                startActivity(i);
            }
        });

        // click function for 9th button
        Button button9 = (Button) findViewById(R.id.latihan9);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(i);
            }
        });

        // click function for 10th button
        Button button10 = (Button) findViewById(R.id.latihan10);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PermActivity.class);
                startActivity(i);
            }
        });
    }
}
