package com.example.fatah.myapplication;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoFragment extends Fragment {
    Context context;

    public PicassoFragment newInstance(Context context){
        PicassoFragment picassoFragment = new PicassoFragment();
        this.context = context;
        return picassoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picasso, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageview_fragment);
        Picasso.with(context).load(R.drawable.android_logo).into(imageView);
        return view;
    }

}
