package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PindahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pindah);

        Button button1 = (Button) findViewById(R.id.button1_pindah);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PindahActivity.this, Pindah2Activity.class);
                startActivity(i);
            }
        });

        Button button2 = (Button) findViewById(R.id.button2_pindah);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = (EditText) findViewById(R.id.edittext_pindah);
                String valString = editText.getText().toString();

                Intent i = new Intent(PindahActivity.this, Pindah2Activity.class);
                i.putExtra("var", valString);
                startActivity(i);
            }
        });
    }
}
