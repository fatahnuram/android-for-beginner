package com.example.fatah.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

import cz.msebera.android.httpclient.Header;

public class RecyclerItemDetailActivity extends AppCompatActivity {
    private String id;
    private AsyncHttpClient client = new AsyncHttpClient();
    private JSONObject object;
    private AQuery query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_item_detail);

        query = new AQuery(this);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");

        String url = "http://ugm.ac.id/berita/detail?id=" + id;

        client.get(this, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody, Charset.forName("UTF8"));

                try {
                    object = new JSONObject(response);

                    query.id(R.id.recycler_detail_nama).text(object.getString("nama"));
                    query.id(R.id.recycler_detail_tgl).text(object.getString("tgl"));
                    query.id(R.id.recycler_detail_img).image(object.getString("gambar"));
                    query.id(R.id.recycler_detail_desc).text(object.getString("desc"));

                }catch (JSONException e){
                    Toast.makeText(RecyclerItemDetailActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(RecyclerItemDetailActivity.this, "Error connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
