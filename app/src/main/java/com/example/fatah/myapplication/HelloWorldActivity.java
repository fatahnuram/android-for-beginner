package com.example.fatah.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

// hello world, this is the default code if we create a new activity
public class HelloWorldActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world);
    }
}
