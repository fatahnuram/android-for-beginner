package com.example.fatah.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by fatah on 07/02/17.
 */

public class RecyclerMovieHolder extends RecyclerView.ViewHolder {
    View item;
    TextView nama, desc, tgl;
    ImageView img;

    public RecyclerMovieHolder(View itemView) {
        super(itemView);
        item = itemView;
        nama = (TextView) itemView.findViewById(R.id.recycler_item_nama);
        desc = (TextView) itemView.findViewById(R.id.recycler_item_desc);
        tgl = (TextView) itemView.findViewById(R.id.recycler_item_tgl);
        img = (ImageView) itemView.findViewById(R.id.recycler_item_img);
    }

    public void bindView(Context context, ArrayList<RecyclerItemMovie> news, int position){
        final RecyclerItemMovie item = news.get(position);

        nama.setText(item.getNama());
        desc.setText(item.getDesc());
        tgl.setText(item.getTgl());

        Picasso.with(context).load(item.getImg()).into(img);

        this.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id", item.getId());
                EventBus.getDefault().post(bundle);
            }
        });
    }
}
