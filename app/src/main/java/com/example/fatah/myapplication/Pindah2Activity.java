package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Pindah2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pindah2);

        Button button = (Button) findViewById(R.id.button3_pindah);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getIntent();
                Bundle bundle = i.getExtras();

                try{
                    String var = bundle.get("var").toString();

                    if(var.length() == 0){
                        throw new NullPointerException();
                    }

                    TextView textView = (TextView) findViewById(R.id.textview);
                    textView.setText(var);
                }catch(NullPointerException e){
                    Toast.makeText(Pindah2Activity.this, "Data kosong om!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
