package com.example.fatah.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by fatah on 26/01/17.
 */

public class ShopListAdapter extends BaseAdapter {
    // variable declarations
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String,String>> arrayList;
    HashMap<String,String> res = new HashMap<>();

    // constructor
    public ShopListAdapter(Context context, ArrayList<HashMap<String,String >> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    // return number of items to be shown
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // function to show item into screen
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // variable view declarations
        TextView textViewRank;
        TextView textViewCountry;
        TextView textViewPopulation;

        // get layout inflater from context
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        // inflate the main layout
        View item = inflater.inflate(R.layout.listview_item, parent, false);
        // get item's hashmap by position
        res = arrayList.get(position);

        // init view
        textViewRank = (TextView) item.findViewById(R.id.textview_rank);
        textViewCountry = (TextView) item.findViewById(R.id.textview_country);
        textViewPopulation = (TextView) item.findViewById(R.id.textview_population);

        // set text from map using each key into view
        textViewRank.setText(res.get("rank"));
        textViewCountry.setText(res.get("country"));
        textViewPopulation.setText(res.get("population"));

        return item;
    }
}
