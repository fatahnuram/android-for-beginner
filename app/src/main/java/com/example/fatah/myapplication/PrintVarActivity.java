package com.example.fatah.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PrintVarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_var);

        // declare and init button
        Button button1 = (Button) findViewById(R.id.button1_printvar);
        // set click function to button
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // declare and init view
                TextView tv = (TextView) findViewById(R.id.helloworldtext);
                // change the text
                tv.setText("Print my name!");
            }
        });

        // declare and init button
        Button button2 = (Button) findViewById(R.id.button2_printvar);
        // set click function to button
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // declare and init view
                TextView tv = (TextView) findViewById(R.id.numtext);
                // declare and init number
                int num = 100;
                // show number into screen
                tv.setText("" + num);
            }
        });
    }
}
