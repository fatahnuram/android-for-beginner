package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SampleFragmentTwo extends Fragment {
    // function for init fragment
    public SampleFragmentTwo newInstance(){
        SampleFragmentTwo sampleFragment = new SampleFragmentTwo();
        return sampleFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        // inflate the parent view
        View rootView = inflater.inflate(R.layout.fragment_sample_fragment_two, container, false);
        return  rootView;
    }
}
