package com.example.fatah.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

// class fragment for fragment number three
public class DrawerFragmentThree extends Fragment {
    // function for init fragment
    public DrawerFragmentThree newInstance() {
        DrawerFragmentThree drawerFragmentThree = new DrawerFragmentThree();
        return drawerFragmentThree;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer_fragment_three, container, false);
    }
}
