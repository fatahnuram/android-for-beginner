package com.example.fatah.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class PicassoListAdapter extends ShopListAdapter {
    public PicassoListAdapter(Context context, ArrayList<HashMap<String, String>> arrayList) {
        super(context, arrayList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textViewRank;
        TextView textViewCountry;
        TextView textViewPopulation;
        ImageView imageView;

        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item = inflater.inflate(R.layout.listview_item_picasso, parent, false);
        res = arrayList.get(position);

        textViewRank = (TextView) item.findViewById(R.id.textview_rank_picasso);
        textViewCountry = (TextView) item.findViewById(R.id.textview_country_picasso);
        textViewPopulation = (TextView) item.findViewById(R.id.textview_population_picasso);
        imageView = (ImageView) item.findViewById(R.id.imageview_fragment_item_picasso);

        textViewRank.setText(res.get("rank"));
        textViewCountry.setText(res.get("country"));
        textViewPopulation.setText(res.get("population"));
        Picasso.with(context).load(res.get("flag")).into(imageView);

        return item;
    }
}
