package com.example.fatah.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DrawerActivity extends AppCompatActivity {
    // variable declarations
    private DrawerListModel[] models;
    private DrawerLayout drawerLayout;
    private ListView listView;

    // register the class
    @Override
    protected void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    // unregister the class
    @Override
    protected void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    // function executed from EventBus
    @Subscribe
    public void onEvent(Bundle bundle){
        // if the bundle contains string "posisi"
        if(bundle.containsKey("posisi")){
            // get the fragment and its manager
            Fragment fragment;
            FragmentManager fragmentManager = getSupportFragmentManager();

            // get the position selected
            switch (bundle.getInt("posisi")){
                case 0:
                    // if 0, select fragment one
                    fragment = new DrawerFragmentOne().newInstance();
                    break;
                case 1:
                    // if 1, select fragment two
                    fragment = new DrawerFragmentTwo().newInstance();
                    break;
                case 2:
                    // if 2, select fragment three
                    fragment = new DrawerFragmentThree().newInstance();
                    break;
                default:
                    // else, select home fragment
                    fragment = new DrawerFragment().newInstance();
                    break;
            }

            // set the fragment selected to the screen
            fragmentManager.beginTransaction().replace(R.id.drawer_content_frame, fragment).commit();
            // close the drawer
            drawerLayout.closeDrawer(listView);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        // declare model number for drawer items
        models = new DrawerListModel[3];
        // get model items' name
        String[] modelName = getResources().getStringArray(R.array.drawer_list_array);
        //int[] modelIcon = getResources().getIntArray(R.array.drawer_icon);

        // init model items' icon
        int[] modelIcon = {R.drawable.connect, R.drawable.fixtures, R.drawable.table};
        // init model into objects
        for(int i=0; i<models.length; i++){
            //Log.i("icon", ""+modelIcon);
            models[i] = new DrawerListModel(modelIcon[i], ""+modelName[i]);
        }

        // init view
        drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer);
        listView = (ListView) findViewById(R.id.drawer_list);

        // set the adapter for drawer
        listView.setAdapter(new DrawerListAdapter(this, models));

        // set the home fragment to screen
        getSupportFragmentManager().beginTransaction().replace(R.id.drawer_content_frame, new DrawerFragment().newInstance()).commit();
    }
}
