package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ConversionActivity extends AppCompatActivity {
    // declare variable
    private TextView textView;
    private Button button;
    private EditText et1, et2;

    private String val1, val2;
    private int int1, int2, hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        // init view
        textView = (TextView) findViewById(R.id.hasilkonversi);

        // init button
        button = (Button) findViewById(R.id.button_konversi);
        // define click function
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // init edittext on view
                et1 = (EditText) findViewById(R.id.edittext1);
                et2 = (EditText) findViewById(R.id.edittext2);

                // exception handling if null
                try{
                    // get text from edittext
                    val1 = et1.getText().toString();
                    val2 = et2.getText().toString();

                    // parse int from string
                    int1 = Integer.parseInt(val1);
                    int2 = Integer.parseInt(val2);

                    // processing conversion
                    hasil = int1 * int2;

                    // show text to screen
                    textView.setText(""+hasil);
                }catch (Exception e){
                    // if null on the edittext
                    Toast.makeText(ConversionActivity.this, "kosong om!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
