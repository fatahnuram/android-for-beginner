package com.example.fatah.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoLanderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picasso_lander);

        ImageView imageView = (ImageView) findViewById(R.id.imageview_lander);
        Picasso.with(this).load(R.drawable.pablo_picasso).into(imageView);

        Button button = (Button) findViewById(R.id.button_picasso);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PicassoLanderActivity.this, PicassoActivity.class);
                startActivity(i);
            }
        });
    }
}
